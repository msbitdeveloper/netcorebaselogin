﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.Google;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using MixedPlace.API.Data;
using MixedPlace.API.Models.User;
using MixedPlace.API.Services;

namespace MixedPlace.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // ===== Add our DbContext ========
            services.AddDbContext<ApplicationDbContext>();

            // ===== Add Identity ========
            services.AddIdentity<User, IdentityRole>(options =>
            {
                options.Password.RequiredUniqueChars = 0;
                options.Password.RequireLowercase = false;
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 4;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
            })
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            // ===== Add Jwt Authentication ========
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear(); // => remove default claims
            services
                .AddAuthentication(
               options =>
               {
                   options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;//CookieAuthenticationDefaults.AuthenticationScheme;
                   options.DefaultSignInScheme = "Temporary";
                   options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
               }
                ).AddGoogle("google", options =>
                {
                    options.ClientId = Configuration["Auth:Google:Clientid"];
                    options.ClientSecret = Configuration["Auth:Google:Clientsecret"];
                }).AddFacebook("facebook", options =>
                {
                    options.AppId = Configuration["Auth:Facebook:Appid"];
                    options.AppSecret = Configuration["Auth:Facebook:Appsecret"];
                })
                .AddJwtBearer(cfg =>
                {
                    cfg.RequireHttpsMetadata = false;
                    cfg.SaveToken = true;
                    cfg.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidIssuer = Configuration["Auth:Jwt:JwtIssuer"],
                        ValidAudience = Configuration["Auth:Jwt:JwtIssuer"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Auth:Jwt:JwtKey"])),
                    };
                })
                .AddCookie("Temporary");

            // ===== Add MVC ========
            services.AddMvc();

            // ===== Add Scoped DI ==
            services.AddScoped<IPasswordHasher<User>, PasswordHasher<User>>();
            services.AddScoped<IAuthService, AuthService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(
            IApplicationBuilder app,
            IHostingEnvironment env,
            ApplicationDbContext dbContext
        )
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // ===== Use Authentication ======
            //TODO: Move to own method that handle the external authentication providers setup

            app.UseAuthentication();
            app.UseMvc();

            // ===== Create tables ======
            //dbContext.Database.EnsureCreated();
        }
    }
}
