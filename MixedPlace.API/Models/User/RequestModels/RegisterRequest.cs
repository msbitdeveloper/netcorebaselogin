﻿using MixedPlace.API.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.User.RequestModels
{
    public class RegisterRequest : UserDto
    {
        [Required]
        public string Username { get; set; }
        [Required]
        public string DeviceID { get; set; }
       
    }
}
