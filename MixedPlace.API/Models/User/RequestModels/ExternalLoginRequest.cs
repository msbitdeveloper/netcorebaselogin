﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.User.RequestModels
{
    public class ExternalLoginRequest
    {
        [Required]
        public string ProviderName { get; set; }
        [Required]
        public string AccessToken { get; set; }
    }
}
