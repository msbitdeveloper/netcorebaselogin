﻿using Microsoft.AspNetCore.Identity;
using System;

namespace MixedPlace.API.Models.User
{
    public class User: IdentityUser
    {
        public bool IsTemporaryPassword { get; set; }
        public DateTime PasswordExpirationDate { get; set; }
        public string DeviceID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Street { get; set; }
        public int StreetNumber { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Gender { get; set; } //TODO: What type is used for this
        public int Age { get; set; }
        public string UserDescription { get; set; }
        public string Image { get; set; } //TODO: What type is used for this



    }
}
