﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Common
{
    public enum InnerErrorCode
    {
        Ok = 0,
        PasswordExipired = 2001,
        ChangePassowrdRequired = 2002,
        InvalidCredentials = 2003,
        UsernameInUse = 2004,
        MixedPlaceAlreadyOwned = 2005,
        MixedPlaceNotOwned = 2006,
        MixedPlaceOwnedByOtherUser = 2007,
        BadRequestArgs = 4001,
        InsufficientBalance = 4002,
        UserNotHasTheModel = 4003,
        UnauthorizedRequest = 4011,
        NotValid = 8000,
        ObjectNotValid = 9997,
        MissingMapping = 9998,
        UnknownError = 9999
    }

    public enum MixedPlaceDesignType
    {
        All = 1,
        SingleItem = 2,
        Composition = 3
    }
}
