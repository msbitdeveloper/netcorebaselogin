﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Common
{
    public interface IResponseModel<T>
    {
        [JsonProperty("statusCode")]
        int StatusCode { get; set; }

        [JsonProperty("innerCode")]
        int InnerCode { get; set; }

        [JsonProperty("description")]
        string Description { get; set; }

        [JsonProperty("data")]
        T Data { get; set; }
    }
}
