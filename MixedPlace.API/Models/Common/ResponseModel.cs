﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MixedPlace.API.Models.Common
{
    public class ResponseModel<T> : IResponseModel<T>
    {
        [JsonProperty("statusCode")]
        public int StatusCode { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("innerCode")]
        public int InnerCode { get; set; } = 0;

        [JsonProperty("data")]
        public T Data { get; set; }

        public ResponseModel() { }

        public ResponseModel(int code, string description, T data = default(T))
        {
            StatusCode = code;
            Description = description;
            Data = data;
        }
    }
}
