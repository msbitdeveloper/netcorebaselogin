﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using MixedPlace.API.Data;
using MixedPlace.API.Models.Common;
using System;
using System.Linq;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authentication;
using MixedPlace.API.Models.User;
using MixedPlace.API.Models.User.ResponseModels;
using MixedPlace.API.Models.User.RequestModels;
using System.Security.Cryptography.X509Certificates;
using MixedPlace.API.Providers;

namespace MixedPlace.API.Services
{
    public class AuthService : IAuthService
    {
        private readonly SignInManager<User> _signInManager;
        private readonly UserManager<User> _userManager;
        private readonly IConfiguration _configuration;
        private readonly IPasswordHasher<User> _passwordHasher;
        private readonly ApplicationDbContext _dbContext;


        public AuthService(
            ApplicationDbContext dbContext,
            UserManager<User> userManager,
            SignInManager<User> signInManager,
            IConfiguration configuration,
            IPasswordHasher<User> passwordHasher)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _configuration = configuration;
            _passwordHasher = passwordHasher;
            _dbContext = dbContext;

        }


        #region Public Methods

        public async Task<IResponseModel<AuthResponse>> Login(LoginRequest loginRequest)
        {

            var response = new ResponseModel<AuthResponse>();
            try
            {
                var user = await _userManager.FindByNameAsync(loginRequest.Username);
                if (user == null)
                {
                    response.StatusCode = (int)InnerErrorCode.InvalidCredentials;
                    return response;
                }

                var verifyResult = _userManager.PasswordHasher.VerifyHashedPassword(user, user.PasswordHash, loginRequest.Password);
                switch (verifyResult)
                {
                    case PasswordVerificationResult.Failed:
                        {
                            response.StatusCode = (int)InnerErrorCode.InvalidCredentials;
                            return response;

                        }
                    case PasswordVerificationResult.SuccessRehashNeeded:
                    case PasswordVerificationResult.Success:
                        {
                            if (user.IsTemporaryPassword)
                            {
                                if (user.PasswordExpirationDate > DateTime.UtcNow)
                                {
                                    response.StatusCode = (int)InnerErrorCode.ChangePassowrdRequired;
                                    return response;
                                }
                                else
                                {
                                    if (GenerateUserTemporaryPassword(user) == false)
                                    {
                                        response.StatusCode = (int)InnerErrorCode.UnknownError;
                                    }
                                    else
                                    {
                                        var result = await _userManager.UpdateAsync(user);
                                        if (result.Succeeded)
                                            response.StatusCode = (int)InnerErrorCode.PasswordExipired;
                                        else
                                            response.StatusCode = (int)InnerErrorCode.UnknownError;
                                    }

                                    return response;
                                }
                            }
                            else
                            {
                                var userTokens =  _dbContext.UserTokens.Where(x => x.UserId == user.Id);
                                if (userTokens != null && userTokens.Count() > 0)
                                {
                                    _dbContext.RemoveRange(userTokens);
                                    _dbContext.SaveChanges();
                                }
                                   

                                var token = GenerateJwtToken(user.Id);
                                var refreshToken = Guid.NewGuid().ToString().Replace("-", "");
                                await _signInManager.SignInAsync(user, false);
                                await _userManager.SetAuthenticationTokenAsync(user, _configuration["Auth:Jwt:Provider"], refreshToken, token);
                                response.StatusCode = (int)InnerErrorCode.Ok;
                                response.Data = new AuthResponse { Token = token, RefreshToken = refreshToken };
                                return response;
                            }

                        }
                }
            }
            catch (Exception exp)
            {
                response.StatusCode = (int)InnerErrorCode.UnknownError;
            }
            return response;
        }

        /// <summary>
        /// Method responsible for validate user access token aginst passed provider.
        /// In case if user authenticated user data existence checked in db.
        /// If user not exist new one created.
        /// JWT token and refresh token are passed to client.
        /// </summary>
        /// <param name="externalLoginRequest"></param>
        /// <returns></returns>
        public async Task<IResponseModel<AuthResponse>> ExternalLogin(ExternalLoginRequest externalLoginRequest)
        {

            var response = new ResponseModel<AuthResponse>();
            try
            {
                var loginProvider = LoginProviderFactory.CreateLoginProvider(externalLoginRequest.ProviderName,_configuration);
                if (loginProvider == null)
                {
                    response.StatusCode = (int)InnerErrorCode.UnknownError;
                    return response;
                }

                var authResult = await loginProvider.AuthenticateToken(externalLoginRequest.AccessToken);
                if (authResult)
                {
                    var userProviderInfo = await loginProvider.GetUserDetials(externalLoginRequest.AccessToken);
                    if (userProviderInfo != null)
                    {
                        var existingUser = await _userManager.FindByNameAsync(userProviderInfo.Email);
                        if (existingUser == null)
                        {
                            existingUser = new User
                            {
                                Email = userProviderInfo.Email,
                                UserName = userProviderInfo.Email,
                                 Image = userProviderInfo.ImageUrl,
                                 Gender = userProviderInfo.Gender,
                                 FirstName = userProviderInfo.FirstName,
                                 LastName = userProviderInfo.LastName,
                                SecurityStamp = Guid.NewGuid().ToString(),
                            };
                            await _userManager.CreateAsync(existingUser);
                        }

                        var loginUser = await _userManager.FindByLoginAsync(externalLoginRequest.ProviderName, userProviderInfo.Id);
                        if (loginUser == null)
                        {
                            var userInfo = new UserLoginInfo(externalLoginRequest.ProviderName, userProviderInfo.Id, userProviderInfo.FullName);
                            await _userManager.AddLoginAsync(existingUser, userInfo);
                        }

                        var token = GenerateJwtToken(existingUser.Id);
                        var refreshToken = Guid.NewGuid().ToString().Replace("-", "");
                        await _signInManager.SignInAsync(existingUser, false);
                        await _userManager.SetAuthenticationTokenAsync(existingUser, _configuration["Auth:Jwt:Provider"], refreshToken, token);
                        response.StatusCode = (int)InnerErrorCode.Ok;
                        response.Data = new AuthResponse { Token = token, RefreshToken = refreshToken };
                        return response;
                    }
                    response.StatusCode = (int)InnerErrorCode.UnknownError;
                    return response;
                }
                response.StatusCode = (int)InnerErrorCode.UnauthorizedRequest;
                return response;
            }
            catch (Exception exp)
            {
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        public async Task<IResponseModel<EmptyBodyResponse>> Logout(string userId)
        {
            var response = new ResponseModel<EmptyBodyResponse>();
            try
            {
                var user = await _userManager.FindByIdAsync(userId);
                if (user == null)
                {
                    response.StatusCode = (int)InnerErrorCode.InvalidCredentials;
                    return response;
                }

                var userTokens = _dbContext.UserTokens.Where(x => x.UserId == userId);
                if (userTokens != null && userTokens.Count() > 0)
                {
                    _dbContext.RemoveRange(userTokens);
                    _dbContext.SaveChanges();
                }
                await _signInManager.SignOutAsync();
                response.StatusCode = (int)InnerErrorCode.Ok;
                return response;
            }
            catch (Exception exp)
            {
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        /// <summary>
        /// Method recieve refresh token and old jwt token, in
        /// case if refresh token valid new jwt token generated and
        /// saved to db
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<IResponseModel<AuthResponse>>RefreshToken(RefeshTokenRequest request)
        {
            var response = new ResponseModel<AuthResponse>();
            try
            {
                var jwtToken = new JwtSecurityToken(request.Token);
                if (string.IsNullOrWhiteSpace(jwtToken.Subject))
                {
                    response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
                    return response;
                }
                var user = await _userManager.FindByIdAsync(jwtToken.Subject);
                if (user == null)
                {
                    response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
                    return response;
                }

                var token = await _userManager.GetAuthenticationTokenAsync(user, _configuration["Auth:Jwt:Provider"], request.RefreshToken);
                if (!string.IsNullOrWhiteSpace(token))
                {
                    await _userManager.RemoveAuthenticationTokenAsync(user, _configuration["Auth:Jwt:Provider"], request.RefreshToken);

                    token = GenerateJwtToken(jwtToken.Subject);
                    var refreshToken = Guid.NewGuid().ToString().Replace("-", "");

                    await _userManager.SetAuthenticationTokenAsync(user, _configuration["Auth:Jwt:Provider"], refreshToken, token);
                    response.StatusCode = (int)InnerErrorCode.Ok;
                    response.Data =  new AuthResponse { Token = token, RefreshToken = refreshToken };
                    return response;
                }
                response.StatusCode = (int)InnerErrorCode.UnauthorizedRequest;
                return response;
            }
            catch (Exception exp)
            {
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        public async Task<IResponseModel<AuthResponse>> ChangePassword(ChangePasswordRequest changePasswordRequest)
        {
            var response = new ResponseModel<AuthResponse>();
            try
            {

                var user = await _userManager.FindByEmailAsync(changePasswordRequest.Email);
                if (user == null)
                {
                    response.StatusCode = (int)InnerErrorCode.InvalidCredentials;
                    return response;
                }

                var verifyResult = _passwordHasher.VerifyHashedPassword(user, user.PasswordHash, changePasswordRequest.OldPassword);
                switch (verifyResult)
                {
                    case PasswordVerificationResult.Failed:
                        response.StatusCode = (int)InnerErrorCode.InvalidCredentials;
                        return response;
                    case PasswordVerificationResult.SuccessRehashNeeded:
                    case PasswordVerificationResult.Success:
                        {
                            var hashedPassword = _passwordHasher.HashPassword(user, changePasswordRequest.NewPassword);
                            user.PasswordHash = hashedPassword;
                            user.IsTemporaryPassword = false;
                            var updateResult = await _userManager.UpdateAsync(user);
                            if (updateResult.Succeeded)
                                response.StatusCode = (int)InnerErrorCode.Ok;
                            else
                                response.StatusCode = (int)InnerErrorCode.UnknownError;
                            break;
                        }
                    default:
                        response.StatusCode = (int)InnerErrorCode.UnknownError;
                        break;
                }
            }
            catch (Exception exp)
            {
                response.StatusCode = (int)InnerErrorCode.UnknownError;
            }

            return response;
        }

        public async Task<IResponseModel<AuthResponse>> ForgotPassword(ForgotPasswordRequest forgotPasswordRequest)
        {
            var response = new ResponseModel<AuthResponse>();
            try
            {
                if (forgotPasswordRequest == null || string.IsNullOrWhiteSpace(forgotPasswordRequest.Email))
                {
                    response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
                    return response;
                }

                var user = await _userManager.FindByEmailAsync(forgotPasswordRequest.Email);
                if (user == null)
                {
                    response.StatusCode = (int)InnerErrorCode.InvalidCredentials;
                    return response;
                }

                if (GenerateUserTemporaryPassword(user))
                {
                    var updateResult = await _userManager.UpdateAsync(user);
                    if (updateResult.Succeeded)
                        response.StatusCode = (int)InnerErrorCode.Ok;
                    else
                        response.StatusCode = (int)InnerErrorCode.UnknownError;
                }
                else
                {
                    response.StatusCode = (int)InnerErrorCode.UnknownError;
                }

            }
            catch (Exception exp)
            {
                response.StatusCode = (int)InnerErrorCode.UnknownError;
            }
            return response;
        }

        public async Task<IResponseModel<AuthResponse>> ObtainToken(string provider, string externalToken)
        {
            var response = new ResponseModel<AuthResponse>();
            try
            {
                if (string.IsNullOrWhiteSpace(provider) || string.IsNullOrWhiteSpace(externalToken))
                {
                    response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
                    return response;
                }

                var verifiedAccessToken = await VerifyExternalToken(provider.ToLower(), externalToken);
                if (verifiedAccessToken == null)
                {
                    response.StatusCode = (int)InnerErrorCode.UnauthorizedRequest;
                    return response;
                }

                var user = await _userManager.FindByLoginAsync(provider, verifiedAccessToken.user_id);
                if (user == null)
                {
                    response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
                    return response;
                }

                var userTokens = _dbContext.UserTokens.Where(x => x.UserId == user.Id);
                if (userTokens != null && userTokens.Count() > 0)
                {
                    _dbContext.RemoveRange(userTokens);
                    _dbContext.SaveChanges();
                }


                var token = GenerateJwtToken(user.Id);
                var refreshToken = Guid.NewGuid().ToString().Replace("-", "");
                await _userManager.SetAuthenticationTokenAsync(user, _configuration["Auth:Jwt:Provider"], refreshToken, token);
                response.StatusCode = (int)InnerErrorCode.Ok;
                response.Data = new AuthResponse { Token = token, RefreshToken = refreshToken };
                return response;
            }
            catch (Exception exp)
            {
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }
        }

        public async Task<IResponseModel<AuthResponse>> Register(RegisterRequest registerRequest)
        {
            var response = new ResponseModel<AuthResponse>();
            try
            {
                if (registerRequest == null)
                {
                    response.StatusCode = (int)InnerErrorCode.BadRequestArgs;
                    return response;
                }

                var existingUser = await _userManager.FindByNameAsync(registerRequest.Username);
                if (existingUser != null)
                {
                    response.StatusCode = (int)InnerErrorCode.UsernameInUse;
                    return response;
                }

                var user = new User
                {
                    Age = registerRequest.Age,
                    City = registerRequest.City,
                    Country = registerRequest.Country,
                    Email = registerRequest.Username,
                    FirstName = registerRequest.FirstName,
                    Gender = registerRequest.Gender,
                    LastName = registerRequest.LastName,
                    Street = registerRequest.Street,
                    StreetNumber = registerRequest.StreetNumber,
                    UserName = registerRequest.Username
                };

                if (GenerateUserTemporaryPassword(user, hashPassword: false))
                {
                    var result = await _userManager.CreateAsync(user, user.PasswordHash);

                    if (result.Succeeded)
                        response.StatusCode = (int)InnerErrorCode.Ok;
                    else
                        response.StatusCode = (int)InnerErrorCode.UnknownError;
                }
                else
                    response.StatusCode = (int)InnerErrorCode.UnknownError;

            }
            catch (Exception exp)
            {
                response.StatusCode = (int)InnerErrorCode.UnknownError;
            }

            return response;
        }

        public async Task<IResponseModel<AuthResponse>> ExternalCallback(HttpContext httpContext)
        {
            var response = new ResponseModel<AuthResponse>();
            try
            {
                var authResult = await httpContext.AuthenticateAsync("Temporary");
                if (authResult.Succeeded)
                {
                    var userEmail = authResult.Principal.FindFirst(ClaimTypes.Email)?.Value;
                    var provider = authResult.Principal.Identity.AuthenticationType;
                    var nameIdentifier = authResult.Principal.FindFirstValue(ClaimTypes.NameIdentifier);
                    var name = authResult.Principal.FindFirstValue(ClaimTypes.Name);
                    var userInfo = new UserLoginInfo(provider, nameIdentifier, name);

                    var existingUser = await _userManager.FindByNameAsync(userEmail);
                    if (existingUser == null)
                    {
                        existingUser = new User
                        {
                            Email = userEmail,
                            UserName = userEmail,
                            SecurityStamp = Guid.NewGuid().ToString(),
                        };
                        await _userManager.CreateAsync(existingUser);
                    }


                    var loginUser = await _userManager.FindByLoginAsync(provider, nameIdentifier);
                    if (loginUser == null)
                        await _userManager.AddLoginAsync(existingUser, userInfo);

                    await httpContext.SignInAsync(authResult.Principal);
                    response.StatusCode = (int)InnerErrorCode.Ok;
                    response.Data = new AuthResponse { Token = GenerateJwtToken(existingUser.Id) };
                    return response;
                }
                else
                {
                    response.StatusCode = (int)InnerErrorCode.UnknownError;
                    return response;
                }
            }
            catch (Exception exp)
            {
                response.StatusCode = (int)InnerErrorCode.UnknownError;
                return response;
            }

        }


        #endregion Public Methods

        #region Private Methods

        /// <summary>
        /// Method responsible for generate and set user
        /// temporary password with expiration date.
        /// Send email if required
        /// </summary>
        /// <param name="user">User object to be updated with new pass</param>
        /// <param name="hashPassword">Indicate if need to hash the password</param>
        /// <param name="sendEmail">Indicate if need to send message with newly pass</param>
        /// <returns></returns>
        private bool GenerateUserTemporaryPassword(User user, bool hashPassword = true)
        {
            var expireDays = Convert.ToDouble(_configuration["Auth:TemporaryPassword:ExpirationDays"]);
            var tempPassLength = Convert.ToInt32(_configuration["Auth:TemporaryPassword:Lenght"]);
            var tempPassword = Convert.ToBoolean(_configuration["Env:IsDev"]) == true ? _configuration["Env:DevPassword"]
                                                                                      : GenerateTemporaryPassword(tempPassLength);
            if (hashPassword)
            {
                var hashedPassword = _passwordHasher.HashPassword(user, tempPassword);
                user.PasswordHash = hashedPassword;
            }
            else
            {
                user.PasswordHash = tempPassword;
            }


            return true;
        }

        /// <summary>
        /// Method generate JWT token with userid inserted to claims
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private string GenerateJwtToken(string userId)
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, userId),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };

            var expires = DateTime.Now.AddDays(Convert.ToDouble(_configuration["Auth:Jwt:JwtExpireDays"]));

            X509Certificate2 cert = new X509Certificate2(_configuration["Auth:Jwt:CertName"], _configuration["Auth:Jwt:CertPass"]);
            SecurityKey signKey = new X509SecurityKey(cert);
            SigningCredentials credentials = new SigningCredentials(signKey, SecurityAlgorithms.RsaSha256);

            var token = new JwtSecurityToken(
                _configuration["Auth:Jwt:JwtIssuer"],
                _configuration["Auth:Jwt:JwtIssuer"],
                claims,
                expires: expires,
                signingCredentials: credentials
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        /// <summary>
        /// Helper method that generate tempory password with
        /// lenght equal to passed by caller
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        private string GenerateTemporaryPassword(int length)
        {
            const string valid = "abcdefghijklmnozABCDEFGHIJKLMNOZ1234567890!@#$%^&*()-=";
            StringBuilder strB = new StringBuilder(100);
            Random random = new Random();
            while (0 < length--)
            {
                strB.Append(valid[random.Next(valid.Length)]);
            }
            return strB.ToString();
        }

        private async Task<ParsedExternalAccessToken> VerifyExternalToken(string provider, string accessToken)
        {
            ParsedExternalAccessToken parsedToken = null;

            var verifyTokenEndPoint = "";

            if (provider == "facebook")
            {
                var appToken = _configuration["Auth:Facebook:AppToken"];
                verifyTokenEndPoint = string.Format("https://graph.facebook.com/debug_token?input_token={0}&access_token={1}", accessToken, appToken);
            }
            else if (provider == "google")
            {
                verifyTokenEndPoint = string.Format("https://www.googleapis.com/oauth2/v1/tokeninfo?access_token={0}", accessToken);
            }
            else
            {
                return null;
            }

            var client = new HttpClient();
            var uri = new Uri(verifyTokenEndPoint);
            var response = await client.GetAsync(uri);

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();

                dynamic jObj = (JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(content);

                parsedToken = new ParsedExternalAccessToken();

                if (provider == "facebook")
                {
                    parsedToken.user_id = jObj["data"]["user_id"];
                    parsedToken.app_id = jObj["data"]["app_id"];

                    if (!string.Equals(_configuration["Auth:Facebook:Appid"], parsedToken.app_id, StringComparison.OrdinalIgnoreCase))
                    {
                        return null;
                    }
                }
                else if (provider == "google")
                {
                    parsedToken.user_id = jObj["user_id"];
                    parsedToken.app_id = jObj["audience"];

                    if (!string.Equals(_configuration["Auth:Google:Clientid"], parsedToken.app_id, StringComparison.OrdinalIgnoreCase))
                    {
                        return null;
                    }

                }

            }

            return parsedToken;
        }

        #endregion Private Methods
    }
}
