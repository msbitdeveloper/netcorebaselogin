﻿using Microsoft.AspNetCore.Http;
using MixedPlace.API.Models.Common;
using MixedPlace.API.Models.User;
using MixedPlace.API.Models.User.RequestModels;
using MixedPlace.API.Models.User.ResponseModels;
using System.Threading.Tasks;

namespace MixedPlace.API.Services
{
    public interface IAuthService
    {
        Task<IResponseModel<AuthResponse>> Login(LoginRequest loginRequest);
        Task<IResponseModel<AuthResponse>> ExternalLogin(ExternalLoginRequest externalLoginRequest);
        Task<IResponseModel<EmptyBodyResponse>> Logout(string userId);
        Task<IResponseModel<AuthResponse>> RefreshToken(RefeshTokenRequest request);
        Task<IResponseModel<AuthResponse>> Register(RegisterRequest model);
        Task<IResponseModel<AuthResponse>> ForgotPassword(ForgotPasswordRequest forgotPasswordRequest);
        Task<IResponseModel<AuthResponse>> ChangePassword(ChangePasswordRequest changePasswordRequest);
        Task<IResponseModel<AuthResponse>> ObtainToken(string provider, string externalToken);
        Task<IResponseModel<AuthResponse>> ExternalCallback(HttpContext context);
    }
}
