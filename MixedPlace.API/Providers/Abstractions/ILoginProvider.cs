﻿using MixedPlace.API.DataTransferObjects;
using System.Threading.Tasks;

namespace MixedPlace.API.Providers.Abstractions
{
    public interface ILoginProvider
    {
        Task<bool> AuthenticateToken(string accessToken);
        Task <ProviderUserInfoDto> GetUserDetials(string accessToken);
    }
}
