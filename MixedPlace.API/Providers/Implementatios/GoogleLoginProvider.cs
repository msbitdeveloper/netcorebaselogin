﻿using Microsoft.Extensions.Configuration;
using MixedPlace.API.DataTransferObjects;
using MixedPlace.API.Providers.Abstractions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Providers.Implementatios
{
    public class GoogleLoginProvider : ILoginProvider
    {
        public IConfiguration _configurations;
        public GoogleLoginProvider(IConfiguration configuration)
        {
            _configurations = configuration;
        }
        public async Task<bool> AuthenticateToken(string accessToken)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    var getResponse = await httpClient.GetAsync($"https://www.googleapis.com/oauth2/v3/tokeninfo?access_token={accessToken}");
                    if (getResponse.IsSuccessStatusCode)
                    {
                        var contentString = await getResponse.Content.ReadAsStringAsync();
                        if (!string.IsNullOrWhiteSpace(contentString))
                        {
                            var authResponse = JsonConvert.DeserializeObject<GooglTokenInfoResponse>(contentString);

                            if (authResponse.EmailVerified &&
                                DateTime.Now.AddTicks(authResponse.ExpireAt) > DateTime.Now &&
                                authResponse.Aud.ToLower() == _configurations["Auth:Google:Clientid"].ToLower())
                            {
                                return true;
                            }
                        }
                    }
                    return false;
                }
            }
            catch (Exception exp)
            {
                return false;
            }
        }

        public async Task<ProviderUserInfoDto> GetUserDetials(string accessToken)
        {

            try
            {
                using (var httpClient = new HttpClient())
                {
                    var getResponse = await httpClient.GetAsync($"https://www.googleapis.com/oauth2/v1/userinfo?access_token={accessToken}");
                    if (getResponse.IsSuccessStatusCode)
                    {
                        var contentString = await getResponse.Content.ReadAsStringAsync();
                        if (!string.IsNullOrWhiteSpace(contentString))
                        {
                            var userInfo = JsonConvert.DeserializeObject<GoogleUsrInfo>(contentString);
                            if (userInfo != null)
                            {
                                var userDto = new ProviderUserInfoDto
                                {
                                    Email = userInfo.Email,
                                    FirstName = userInfo.FirstName,
                                    FullName = userInfo.Name,
                                    Id = userInfo.Id,
                                    LastName = userInfo.LastName,
                                    Gender = userInfo.Gender,
                                    ImageUrl = userInfo.PictureUrl
                                };

                                return userDto;
                            }
                        }
                    }
                    return null;
                }
            }
            catch (Exception exp)
            {
                return null;
            }
        }
    }


    class GooglTokenInfoResponse
    {
        [JsonProperty(PropertyName = "azp")]
        public string AZP { get; set; }

        [JsonProperty(PropertyName = "aud")]
        public string Aud { get; set; }

        [JsonProperty(PropertyName = "sub")]
        public string Sub { get; set; }

        [JsonProperty(PropertyName = "scope")]
        public string Scope { get; set; }

        [JsonProperty(PropertyName = "exp")]
        public int ExpireAt { get; set; }

        [JsonProperty(PropertyName = "expires_in")]
        public int ExpireIn { get; set; }

        [JsonProperty(PropertyName = "email")]
        public string Email { get; set; }

        [JsonProperty(PropertyName = "email_verified")]
        public bool EmailVerified { get; set; }

        [JsonProperty(PropertyName = "access_type")]
        public string AccessType { get; set; }

    }

    class GoogleUsrInfo
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "email")]
        public string Email { get; set; }

        [JsonProperty(PropertyName = "verified_email")]
        public bool EmailVerified { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "given_name")]
        public string FirstName { get; set; }

        [JsonProperty(PropertyName = "family_name")]
        public string LastName { get; set; }

        [JsonProperty(PropertyName = "link")]
        public string Link { get; set; }

        [JsonProperty(PropertyName = "picture")]
        public string PictureUrl { get; set; }

        [JsonProperty(PropertyName = "gender")]
        public string Gender { get; set; }

        [JsonProperty(PropertyName = "locale")]
        public string Locale { get; set; }

    }
}
