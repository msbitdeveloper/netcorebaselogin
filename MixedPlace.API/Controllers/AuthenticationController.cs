﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using MixedPlace.API.Data;
using MixedPlace.API.Models;
using MixedPlace.API.Models.Common;
using MixedPlace.API.Models.User;
using MixedPlace.API.Models.User.RequestModels;
using MixedPlace.API.Models.User.ResponseModels;
using MixedPlace.API.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlace.API.Controllers
{
    public class AuthenticationController : BaseController
    {
        private readonly SignInManager<User> _signInManager;
        private readonly UserManager<User> _userManager;
        private readonly IConfiguration _configuration;
        private readonly IPasswordHasher<User> _passwordHasher;
        private readonly IAuthService _authService;

        public AuthenticationController(
            UserManager<User> userManager,
            SignInManager<User> signInManager,
            IConfiguration configuration,
            IPasswordHasher<User> passwordHasher,
            IAuthService authService
            )
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _configuration = configuration;
            _passwordHasher = passwordHasher;
            _authService = authService;
        }

        [HttpPost("api/authentication/register")]
        public async Task<IActionResult> Register([FromBody]RegisterRequest registerRequest)
        {
            IResponseModel<AuthResponse> result;
            if (ModelState.IsValid)
            {
                result = await _authService.Register(registerRequest);
                return CreateHttpResponse(result);
            }
            result = new ResponseModel<AuthResponse> { StatusCode = (int)InnerErrorCode.BadRequestArgs };
            return CreateHttpResponse(result);
        }

        [HttpPost("api/authentication/login")]
        public async Task<IActionResult> Login([FromBody]LoginRequest loginRequest)
        {
            IResponseModel<AuthResponse> result;
            if (ModelState.IsValid)
            {
                result = await _authService.Login(loginRequest);
                return CreateHttpResponse(result);
            }
            result = new ResponseModel<AuthResponse> { StatusCode = (int)InnerErrorCode.BadRequestArgs };
            return CreateHttpResponse(result);
        }

        [HttpPost("api/authentication/externallogin")]
        public async Task<IActionResult> ExternalLogin([FromBody]ExternalLoginRequest externalLoginRequest)
        {

            IResponseModel<AuthResponse> result;
            if (ModelState.IsValid)
            {
                result = await _authService.ExternalLogin(externalLoginRequest);
                return CreateHttpResponse(result);
            }
            result = new ResponseModel<AuthResponse> { StatusCode = (int)InnerErrorCode.BadRequestArgs };
            return CreateHttpResponse(result);
        }

        [Authorize]
        [HttpPost("api/authentication/logout")]
        public async Task<IActionResult> Logout()
        {
            var result = await _authService.Logout(GetUserIdFromCliams());
            return CreateHttpResponse(result);
        }

        [HttpPost("api/authentication/refreshtoken")]
        public async Task<IActionResult> RefreshToken([FromBody]RefeshTokenRequest request)
        {
            IResponseModel<AuthResponse> result;
            if (ModelState.IsValid)
            {
                result = await _authService.RefreshToken(request);
                return CreateHttpResponse(result);
            }
            result = new ResponseModel<AuthResponse> { StatusCode = (int)InnerErrorCode.BadRequestArgs };
            return CreateHttpResponse(result);
        }

        [HttpPost("api/authentication/forgotpassword")]
        public async Task<IActionResult> ForgotPassword([FromBody]ForgotPasswordRequest forgotPasswordRequest)
        {
            IResponseModel<AuthResponse> result;
            if (ModelState.IsValid)
            {
                result = await _authService.ForgotPassword(forgotPasswordRequest);
                return CreateHttpResponse(result);
            }
            result = new ResponseModel<AuthResponse> { StatusCode = (int)InnerErrorCode.BadRequestArgs };
            return CreateHttpResponse(result);

        }

        [HttpPost("api/authentication/changepassword")]
        public async Task<IActionResult> ChangePassword([FromBody]ChangePasswordRequest changePasswordRequest)
        {
            IResponseModel<AuthResponse> result;
            if (ModelState.IsValid)
            {
                result = await _authService.ChangePassword(changePasswordRequest);
                return CreateHttpResponse(result);
            }
            result = new ResponseModel<AuthResponse> { StatusCode = (int)InnerErrorCode.BadRequestArgs };
            return CreateHttpResponse(result);

        }

        [HttpGet("api/authentication/getexternallogin/{provider}")]
        public IActionResult GetExternalLogin(string provider)
        {

            if (string.IsNullOrWhiteSpace(provider))
                return BadRequest();

            return Challenge(new AuthenticationProperties
            {
                RedirectUri = $"{Request.Scheme}://{Request.Host}/api/authentication/externalcallback"
            }, provider.ToLower());
        }

        [HttpGet("api/authentication/obtaintoken")]
        public async Task<IActionResult> ObtainToken(string provider, string externalToken)
        {
            var result = await _authService.ObtainToken(provider, externalToken);
            return CreateHttpResponse(result);
        }

        [HttpGet("api/authentication/externalcallback")]
        public async Task<IActionResult> ExternalCallback()
        {
            var result = await _authService.ExternalCallback(HttpContext);
            return CreateHttpResponse(result);
        }



    }
}

