﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MixedPlace.API.Controllers
{
    [Produces("application/json")]
    public class ProtectedController : Controller
    {
        [Authorize]
        [HttpGet("api/protected/protected")]
        public string Protected()
        {
            return "OK";
        }
    }

  
}