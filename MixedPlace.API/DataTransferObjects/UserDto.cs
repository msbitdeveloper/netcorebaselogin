﻿using System.ComponentModel.DataAnnotations;

namespace MixedPlace.API.DataTransferObjects
{
    public class UserDto
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string Street { get; set; }
        [Required]
        public int StreetNumber { get; set; }
        [Required]
        public string City { get; set; }
        [Required]
        public string Country { get; set; }
        [Required]
        public string Gender { get; set; } //TODO: What type is used for this
        [Required]
        public int Age { get; set; }
        [Required]
        public string UniqueID { get; set; }
        [Required]
        public string UserDescription { get; set; }
        [Required]
        public string Image { get; set; } //TODO: What type is used for this
    }
}
